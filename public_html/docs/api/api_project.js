define({
  "name": "WebApp Backend API",
  "version": "1.0.0",
  "description": "Web service to communicate with the backend.",
  "url": "",
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2021-07-27T09:41:36.408Z",
    "url": "https://apidocjs.com",
    "version": "0.28.1"
  }
});
