<?php

namespace Tests\Feature\AutoGen\API\V1;

use Tests\TestCase;
use App\Entities\Articles\Article;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ArticlesAPISearchArticlesAPIAPITest extends APIBaseTestCase
{
    use DatabaseTransactions;

    /**
     *
     *
     *
     * @return  void
     */
    public function test_api_articlesapi_get_search_articlesapi()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/articles', $headers);

        $this->saveResponse($response->getContent(), 'articlesapi_get_search_articlesapi', $response->getStatusCode());

        $response->assertStatus(200);
    }

    public function test_api_get_articles_search()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        Article::truncate();
        Article::factory()->count(10)->create();
        Article::factory()->create([
            'title' => 'No perfume will stink.'
        ]);

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/articles?q=perfume', $headers);

        $this->saveResponse($response->getContent(), 'article_get_get_articles', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [['title' => 'No perfume will stink.']]]);
    }

    public function test_api_get_articles_pagination()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        Article::truncate();
        Article::factory()->count(50)->create();

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/articles?page=3&per_page=10', $headers);

        $this->saveResponse($response->getContent(), 'article_get_get_articles', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJsonCount(10, 'payload');
        $response->assertJson(['paginator' => ['current_page' => 3]]);
    }

    public function test_api_get_articles_data_types()
    {
        $data = $cookies = $files = $headers = $server = [];
        $faker = \Faker\Factory::create('en_AU');
        $content = null;

        Article::truncate();
        $article = Article::factory()->create([
            'title' => 'No perfume will stink.'
        ]);

        // header params
        $headers['Accept'] = 'application/json';
        $headers['x-access-token'] = $this->getAccessToken();
        $headers['x-api-key'] = $this->getApiKey();


        $response = $this->get('/api/v1/articles', $headers);

        $this->saveResponse($response->getContent(), 'article_get_get_articles', $response->getStatusCode());

        $response->assertStatus(200);
        $response->assertJson(['payload' => [['id' => (string) $article->id]]], true);
    }
}
