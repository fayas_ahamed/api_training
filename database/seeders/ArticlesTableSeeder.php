<?php

namespace Database\Seeders;

use App\Entities\Articles\Article;
use Illuminate\Database\Seeder;

class ArticlesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Article::factory()->count(50)->create();
    }
}
