<?php

namespace App\Entities\Articles;

use App\Entities\BaseRepository;

class ArticlesRepository extends BaseRepository
{

	public function __construct(Article $model)
	{
		parent::__construct($model);
	}

}
