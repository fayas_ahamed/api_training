<?php

namespace App\Entities\Articles;

use Database\Factories\ArticleFactory;
use Illuminate\Database\Eloquent\Model;
use EMedia\Formation\Entities\GeneratesFields;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use ElegantMedia\SimpleRepository\Search\Eloquent\SearchableLike;

class Article extends Model
{

    use HasFactory;
    use SearchableLike;
    use GeneratesFields;

    // use \Cviebrock\EloquentSluggable\Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    /*
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];
    }
    */

    protected $fillable = [
        'id',
        'title',
        'body'
    ];


    protected $casts = [
        'id' => 'string'
    ];

    protected $searchable = [
        'title'
    ];

    protected $editable = [
        'title',
    ];

    /**
     *
     * Add any update only validation rules for this model
     *
     * @return array
     */
    public function getCreateRules()
    {
        return [
            'name' => 'required',
        ];
    }

    /**
     *
     * Add any update only validation rules for this model
     *
     * @return array
     */
    public function getUpdateRules()
    {
        return [
            'name' => 'required',
        ];
    }

    /**
     *
     * Add any update only validation messations
     *
     * @return array
     */
    public function getCreateValidationMessages()
    {
        return [];
    }

    /**
     *
     * Add any update only validation messations
     *
     * @return array
     */
    public function getUpdateValidationMessages()
    {
        return [];
    }

    protected static function newFactory()
    {
        return ArticleFactory::new();
    }
}
