<?php

namespace App\Http\Controllers\API\V1;

use App\Entities\Articles\Article;
use App\Entities\Articles\ArticlesRepository;
use App\Http\Controllers\API\V1\APIBaseController;
use EMedia\Api\Docs\APICall;
use Illuminate\Http\Request;

class ArticlesAPIController extends APIBaseController
{

	protected $repo;

	public function __construct(ArticlesRepository $repo)
	{
		$this->repo = $repo;
	}

	protected function index(Request $request)
	{
		document(function () {
                	return (new APICall())
                	    ->setParams([
                	        'q|Search query',
                	        'page|Page number',
                            'per_page|Number of items per page',
                        ])
                        ->setApiKeyHeader()
                        ->setSuccessPaginatedObject(Article::class);
                });

        $filter = $this->repo->newSearchFilter();
        $filter->setPerPage($request->query('per_page',10));
		$items = $this->repo->search($filter);

		return response()->apiSuccessPaginated($items);
	}

}
